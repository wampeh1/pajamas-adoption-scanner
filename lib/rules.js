import { parse } from 'yaml';
import { globby } from 'globby';

import { join, dirname, sep } from 'node:path';
import { fileURLToPath } from 'node:url';
import { readFile } from 'node:fs/promises';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

async function loadRules(filePath) {
  const { rules } = parse(await readFile(join(__dirname, '..', filePath), 'utf-8'));

  return rules.map(({ id, ...rule }) => ({
    id: ['pas', ...dirname(filePath).split(sep), id].join('.'),
    ...rule,
  }));
}

function getAllRulePaths() {
  return globby(['rules/**/*.yml'], { cwd: join(__dirname, '..') });
}

/**
 * Normalizes a finding's `check_id` to `pas.<...dirs>.<id>`.
 *
 * The main purpose of this is to remove environment-specific directory
 * hierarchy from findings' `check_id`s, so that they are directly comparable
 * with the combined rule file's rule ids.
 *
 * @param {string} checkId A finding's `check_id`
 * @returns {string} A normalized, fully-qualified
 */
export function normalizeCheckId(checkId) {
  const token = '.rules.components.';
  const index = checkId.indexOf(token);

  if (index < 0) throw new Error(`Invalid check_id, does not contain "${token}": ${checkId}`);

  return `pas${checkId.slice(index)}`;
}

/**
 * Loads and returns all rules and extends the ID with a prefix (pas) and a directory path of the file with rules,
 *  e.g. a rule with the id `baz-schmatz` in a file rules/foo/bar.yml will have the following ID:
 * `pas.rules.foo.baz-schmatz`
 *
 * @returns {Promise<Array>} Array of semgrep rules
 */
export async function getAllRules() {
  const paths = await getAllRulePaths();
  return (await Promise.all(paths.map(loadRules))).flat();
}
