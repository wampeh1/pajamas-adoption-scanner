import { fileURLToPath } from 'node:url';
import { readFile } from 'node:fs/promises';
import { join } from 'node:path';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

async function loadPathToGroupMap(filepath) {
  const groupToPaths = JSON.parse(await readFile(filepath, 'utf-8'));
  const duplicates = new Map();
  const pathToGroupMap = Object.entries(groupToPaths).reduce((acc, [group, paths]) => {
    for (const path of paths) {
      if (acc.has(path)) {
        if (!duplicates.has(path)) {
          duplicates.set(path, [acc.get(path)]);
        }

        duplicates.get(path).push(group);
      }

      acc.set(path, group);
    }

    return acc;
  }, new Map());

  if (duplicates.size > 0) {
    const duplicatesMessage = Array.from(duplicates.entries())
      .reduce((acc, [path, groups]) => {
        acc.push(`${path} is in ${groups.length} groups: ${groups.join(', ')}`);
        return acc;
      }, [])
      .join('\n');
    throw new Error(`Some file paths have multiple groups:\n\n${duplicatesMessage}\n`);
  }

  return pathToGroupMap;
}

const pathToGroupMap = await loadPathToGroupMap(join(__dirname, 'group_to_paths.json'));

const DEFAULT_GROUP = 'group::unknown';

/**
 * Returns the group label given a file path.
 *
 * It looks up the exact match first, and then lops off path components until
 * a match is found.
 *
 * @param {string} path A file path
 * @returns {string} A group label
 */
export function groupForPath(path) {
  let partialPath = path;

  // eslint-disable-next-line no-constant-condition
  while (true) {
    if (pathToGroupMap.has(partialPath)) return pathToGroupMap.get(partialPath);

    // Have we exhausted all path segments?
    if (partialPath.startsWith('.')) return DEFAULT_GROUP;

    // Lop off another path segment for next iteration.
    partialPath = join(partialPath, '..');
  }
}

/**
 * Returns the group metrics summary for a given scan file.
 *
 * @param {string} filePath A file path
 * @returns {Promise<Object>} The group metrics summary
 */
export async function groupMetricsForScan(filePath) {
  const scan = JSON.parse(await readFile(filePath, 'utf-8'));

  // NOTE: This is similar to groupRulesTableDataProps in
  // dashboard/lib/rules_table.js. It's different enough that extracting
  // a reusable util probably doesn't make sense.
  const itemsMap = [...scan.results].reduce((acc, finding) => {
    const { group } = finding.extra;
    if (!acc.has(group))
      acc.set(group, {
        name: group,
        notAdopted: 0,
        adopted: 0,
      });

    const item = acc.get(group);

    // Note: Warnings are ignored, since by definition they do not have
    // migration paths, i.e., are not actionable.
    if (finding.extra.severity === 'INFO') {
      item.adopted += 1;
    } else if (finding.extra.severity === 'ERROR') {
      item.notAdopted += 1;
    }

    return acc;
  }, new Map());

  const collator = new Intl.Collator('en');

  return {
    aggregatedAt: new Date().toISOString(),
    minimumFindings: 10,
    bounds: {
      low: 0.8,
      high: 0.95,
    },
    groups: [...itemsMap.values()].sort((a, b) => collator.compare(a.name, b.name)),
  };
}
