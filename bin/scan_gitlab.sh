#!/bin/bash

# shellcheck source=bin/common.sh
source "$(dirname "${BASH_SOURCE[0]}")"/common.sh

REF="${1:-HEAD}"

print_help() {
  cat <<HELP_MESSAGE
Usage: $(basename "$0") [REF [SEMGREP_OPTIONS...]]

Scans the GitLab repository at commit REF, which defaults to HEAD.

Runs a scan on "$REPO" at commit REF. Arguments after REF are passed to \
semgrep. This means that REF is required if passing arguments to semgrep.

$(common_help)
HELP_MESSAGE
}

for arg in "$@"; do
  if [ "$arg" = "-h" ] || [ "$arg" = "--help" ]; then
    print_help
    exit
  fi
done

if [ "$#" -gt 0 ]; then
  # Drop REF argument so we can pass remaining arguments to semgrep
  shift 1
fi

cd "$REPO"
git checkout "$REF"
semgrep \
  --config "$ROOT_DIR/rules" \
  --exclude "app/assets/javascripts/ide/" \
  --exclude "app/views/shared/issuable/_search_bar.html.haml" \
  --exclude "app/views/shared/projects/_search_bar.html.haml" \
  {,ee/}app \
  "$@"
