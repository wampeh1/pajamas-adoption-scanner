#!/bin/bash

# shellcheck source=bin/common.sh
source "$(dirname "${BASH_SOURCE[0]}")"/common.sh

semgrep --version

# Rules cannot be validated in CI without a semgrep token
# Locally it works though
if [ -z "$CI" ]; then
  warn "Not running in CI, validating rules..."

  semgrep --validate --config rules/
fi

semgrep --test rules
