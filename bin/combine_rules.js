#!/usr/bin/env node

import { stringify } from 'yaml';

import { fileURLToPath } from 'node:url';
import { join, dirname } from 'node:path';
import { writeFile, mkdir } from 'node:fs/promises';

import { getAllRules } from '../lib/rules.js';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

const rules = await getAllRules();

const targetYamlFile = join(__dirname, '..', 'tmp/pas-rules.yml');
const targetJsonFile = join(__dirname, '..', 'tmp/rules.json');

await mkdir(dirname(targetYamlFile), { recursive: true });

await writeFile(targetYamlFile, stringify({ rules }), 'utf-8');
await writeFile(targetJsonFile, JSON.stringify(rules), 'utf-8');
