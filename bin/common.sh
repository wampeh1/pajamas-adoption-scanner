# This file defines shared options, variables and functions for scripts. It is
# not meant to be called directly (hence it is not executable).
#
# Ignore unused variables; they are used in scripts that source this one.
# shellcheck disable=SC2034
# shellcheck shell=bash

set -euo pipefail
IFS=$'\n\t'

ROOT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")/.." &>/dev/null && pwd)

CI="${CI:-}"

REPO="$ROOT_DIR/gitlab"
MAIN_BRANCH=master
ORIGINAL_REF=

export SEMGREP_SEND_METRICS=off
export SEMGREP_ENABLE_VERSION_CHECK=0

mkdir -p "$ROOT_DIR/tmp/history"

warn() {
  printf >&2 '%s\n' "$*"
}

fail() {
  warn "$*"
  exit 1
}

common_help() {
  cat <<HEREDOC
To run this locally, first symlink the GitLab repository to "$REPO". \
For example:

    ln -s <path_to_gitlab_repo> $REPO

OPTIONS
  -h, --help     print this help message
HEREDOC
}

ensure_gitlab_repo() {
  if [ -n "$CI" ]; then
    warn "Cloning gitlab@${MAIN_BRANCH} into $REPO"
    git clone https://gitlab.com/gitlab-org/gitlab.git \
      "$REPO" \
      "$@" \
      --branch "$MAIN_BRANCH"
    cd "$REPO"
  else
    warn "Not running in CI, so assuming gitlab dir/link already exists..."

    cd "$REPO" || fail "Have you run \`ln -s <path_to_gitlab> gitlab\`?"

    local status
    status=$(git status --porcelain)
    if [ -n "$status" ]; then
      warn "The GitLab repository isn't clean. Please commit or stash the changes listed before trying again."
      fail "$status"
    fi

    ORIGINAL_REF="$(git rev-parse --symbolic --abbrev-ref @)"
    warn "Stored original ref $ORIGINAL_REF"
    git fetch origin "$MAIN_BRANCH"
  fi
}

attempt_to_restore_original_git_checkout() {
  [ -z "$ORIGINAL_REF" ] && return 0

  cd "$REPO"
  warn "Restoring repo checkout to $ORIGINAL_REF..."
  git checkout "$ORIGINAL_REF" || true
}
