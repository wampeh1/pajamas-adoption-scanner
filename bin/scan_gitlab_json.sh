#!/bin/bash

# shellcheck source=bin/common.sh
source "$(dirname "${BASH_SOURCE[0]}")"/common.sh

REF="${1:-HEAD}"

SEMGREP_RESULTS_JSON="$ROOT_DIR/tmp/current_DATE_COMMITHASH.json"

print_help() {
  cat <<HELP_MESSAGE
Usage: $(basename "$0") [REF]

Scans the GitLab repository at commit REF, which defaults to HEAD, and dumps \
the JSON results to $SEMGREP_RESULTS_JSON.

$(common_help)
HELP_MESSAGE
}

for arg in "$@"; do
  if [ "$arg" = "-h" ] || [ "$arg" = "--help" ]; then
    print_help
    exit
  fi
done

ensure_gitlab_repo --depth=1

DATE=$(git show "$REF" --no-patch --pretty=format:%as)
COMMITHASH=$(git rev-parse "$REF")
SEMGREP_RESULTS_JSON=${SEMGREP_RESULTS_JSON/DATE/"$DATE"}
SEMGREP_RESULTS_JSON=${SEMGREP_RESULTS_JSON/COMMITHASH/"$COMMITHASH"}

# Before this script terminates, attempt to restore the GitLab repository to
# the commit/ref it was checked out to before the script was run.
# Has no effect in CI.
trap attempt_to_restore_original_git_checkout EXIT

# Scan REF, write JSON results
"$ROOT_DIR"/bin/scan_gitlab.sh \
  "$REF" \
  --json \
  --output "$SEMGREP_RESULTS_JSON"
