#!/usr/bin/env node

import { fileURLToPath } from 'node:url';
import { join, dirname, basename, relative } from 'node:path';
import { readFile, writeFile, mkdir, readdir } from 'node:fs/promises';
import merge from 'lodash/merge.js';
import { normalizeCheckId } from '../lib/rules.js';
import { groupForPath, groupMetricsForScan } from '../lib/groups.js';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

const outDir = join(__dirname, '..', 'tmp');
const historyDir = join(outDir, 'history');
await mkdir(historyDir, { recursive: true });

/**
 * Write text to given file path.
 *
 * The file path is interpreted as relative to the temporary `outDir`.
 *
 * @param {string} filePath The path for the file to write.
 * @param {string} text The text to write to the file.
 * @returns {Promise<void>}
 */
async function write(filePath, text) {
  const filePathInOutDir = join(outDir, filePath);

  await mkdir(dirname(filePathInOutDir), { recursive: true });

  return writeFile(filePathInOutDir, text, 'utf-8');
}

/**
 * Normalizes a scan file and rewrites it in place.
 *
 * At the moment, that means normalizing the `check_id` of each finding.
 *
 * @param {string} filePath The path to a scan file.
 * @returns {Promise<void>}
 */
async function normalizeScanFile(filePath) {
  const parsed = JSON.parse(await readFile(filePath, 'utf-8'));
  parsed.results = parsed.results.map((result) =>
    merge({}, result, {
      // eslint-disable-next-line camelcase
      check_id: normalizeCheckId(result.check_id),
      extra: {
        group: groupForPath(result.path),
      },
    }),
  );

  return writeFile(filePath, JSON.stringify(parsed), 'utf-8');
}

/**
 * Return a list of absolute file paths in the given directory that satisfy the
 * predicate function.
 *
 * @param {string} dir The directory path to read.
 * @param {Function} predicate Predicate function with which to filter paths.
 * @returns {string[]} List of absolute file paths.
 */
async function getAbsolutePaths(dir, predicate) {
  return (await readdir(dir)).filter(predicate).map((path) => join(dir, path));
}

/**
 * Return a summary of the JSON scan result at the given path.
 *
 * The file name must have the structure arbitrary_labels_date_commithash.json,
 * where the arbitrary labels are optional (and ignored). Example valid paths:
 *
 * - current_2022-01-01_123abc.json
 * - 2021-12-25_abc123.json
 * - __foo_bar__2021-12-25_abc123.json
 *
 * @param {string} filePath The absolute path to a Semgrep JSON scan result.
 * @returns {Object} Object summarizing the scan result with some metadata.
 */
async function summarizeScan(filePath) {
  const { results } = JSON.parse(await readFile(filePath, 'utf-8'));

  const [date, commit] = basename(filePath, '.json').split('_').slice(-2);
  const summary = {};
  const compliance = { compliant: 0, defiant: 0 };

  for (const result of results) {
    const { check_id: ruleId, extra } = result;

    if (extra?.metadata?.pajamasCompliant) {
      compliance.compliant += 1;
    } else {
      compliance.defiant += 1;
    }
    summary[ruleId] ||= 0;
    summary[ruleId] += 1;
  }

  return {
    date,
    commit,

    // This relative path is expected to be valid with respect to the
    // dashboard's <base href>.
    file: relative(outDir, filePath),
    summary,
    compliance,
  };
}

// Collect scan file paths
const [currentScanPath] = await getAbsolutePaths(
  outDir,
  (path) => path.startsWith('current') && path.endsWith('.json'),
);
const historicalScanPaths = await getAbsolutePaths(historyDir, (path) => path.endsWith('.json'));

// Normalize them
await Promise.all([currentScanPath, ...historicalScanPaths].map(normalizeScanFile));

// Summarize them
const current = await summarizeScan(currentScanPath);
const history = await Promise.all(historicalScanPaths.sort().map(summarizeScan));

// Write out the summary
await write('scans.json', JSON.stringify({ current, history }));

// Write out group metrics, to be used by Snowflake/Tableau.
// See https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/5348.
await write('adoption_by_group.json', JSON.stringify(await groupMetricsForScan(currentScanPath)));

function CSVify(results) {
  return [
    ['date', 'Pajamas compliant', 'Pajamas non-compliant'],
    ...results.map(({ date, compliance }) => [date, compliance.compliant, compliance.defiant]),
  ]
    .map((line) => `"${line.join('","')}"`)
    .join('\n');
}

await write('history.csv', CSVify(history));
