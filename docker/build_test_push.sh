#!/bin/sh
set -eu

CI_REGISTRY=${CI_REGISTRY:-}
CI_REGISTRY_USER=${CI_REGISTRY_USER:-}
CI_REGISTRY_PASSWORD=${CI_REGISTRY_PASSWORD:-}
IMAGE=${IMAGE:-}
SEMGREP_VERSION=${SEMGREP_VERSION:-}
TEST_DIR=./docker/test

TEMP_IMAGE=${CI_COMMIT_SHORT_SHA:-}

docker login --username "$CI_REGISTRY_USER" --password "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
docker build --file docker/Dockerfile --tag "$TEMP_IMAGE" --build-arg "SEMGREP_VERSION=${SEMGREP_VERSION}" .
docker run --rm --volume ${TEST_DIR}:/test --workdir /test "$TEMP_IMAGE" scan_gitlab_code_quality /test

# For debugging
ls -lh "$TEST_DIR"

if [ ! -s "${TEST_DIR}/gl-code-quality-report.json" ]; then
  echo "Empty or no Code Quality report file detected. Aborting push of $IMAGE to registry!"
  exit 1
fi

if [ ! -s "${TEST_DIR}/pas-findings.json" ]; then
  echo "Empty or no Semgrep JSON file detected. Aborting push of $IMAGE to registry!"
  exit 1
fi

docker tag "$TEMP_IMAGE" "$IMAGE"

# If we've gotten to this point without error (thanks to `set -e`), we've
# successfully built and tested the image. Now we can push it.
docker push "$IMAGE"
