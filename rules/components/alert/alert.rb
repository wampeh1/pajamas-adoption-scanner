- # ruleid: alert-callout-class-haml-rb
content_tag(:div, class: 'bs-callout bs-callout-warning') do
  content_tag(:details) do
    concat content_tag(:summary, message, class: 'gl-mb-2')
    warning_markdown(pipeline) { |markdown| concat markdown }
  end
end
