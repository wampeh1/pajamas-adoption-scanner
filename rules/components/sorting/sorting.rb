# ok: custom-sorting-haml-rb
def sort_direction_button(reverse_url, reverse_sort, sort_value) end

# ok: custom-sorting-haml-rb
def forks_sort_direction_button(sort_value, without = [:state, :scope, :label_name, :milestone_id, :assignee_id, :author_id])
  reverse_sort = forks_reverse_sort_options_hash[sort_value]
  url = page_filter_path(sort: reverse_sort, without: without)

  # ruleid: custom-sorting-haml-rb
  sort_direction_button(url, reverse_sort, sort_value)
end

# ruleid: custom-sorting-haml-rb
wiki_sort_controls(wiki, sort, direction)

# ok: custom-sorting-haml-rb
def wiki_sort_controls(wiki, sort, direction) end
