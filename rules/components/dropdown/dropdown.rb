# ruleid: bootstrap-dropdown-toggle-rb
options = {
  toggle: 'dropdown',
  id: 'foo'
}

def foo(title, options)
  # ruleid: dropdowns-helper-haml-rb
  dropdown_tag(title, options: options)
end

def bar(title, options)
  # ruleid: dropdowns-helper-haml-rb
  dropdown_toggle(title, options)
end
