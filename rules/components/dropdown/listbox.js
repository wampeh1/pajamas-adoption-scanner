// ruleid: listbox-users-select
import UsersSelect from '~/users_select';
// ruleid: listbox-deprecated-jquery-dropdown
import initDeprecatedJQueryDropdown from '~/deprecated_jquery_dropdown';

import TimezoneDropdown, {
  formatTimezone,
// ruleid: listbox-timezone-dropdown
} from '~/pages/projects/pipeline_schedules/shared/components/timezone_dropdown';

// ruleid: listbox-timezone-dropdown
import TimezoneDropdown from './components/timezone_dropdown';

// okay: listbox-timezone-dropdown
import TimezoneDropdown from './components/timezone_dropdown.vue';

import TimezoneDropdown
// okay: listbox-timezone-dropdown
from './components/timezone_dropdown.vue';

// okay: listbox-timezone-dropdown
const foo = 'hey/timezone_dropdown'

// ruleid: listbox-selecttwo
import 'select2/select2';

// ruleid: listbox-selecttwo
import(/* webpackChunkName: 'select2' */ 'select2/select2');