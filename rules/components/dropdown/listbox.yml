rules:
  - id: okay-listbox-vue-component
    patterns:
      - pattern-inside: import ... from '@gitlab/ui'
      - pattern-either:
        - pattern: GlListbox
        - pattern: GlCollapsibleListbox
    message: Correct usage of GlListbox @gitlab/ui component.
    languages:
      - generic
    severity: INFO
    paths:
      include:
        - "*.js"
        - "*.vue"
      exclude:
        # Ignore the client-side helper for gl_redirect_listbox_tag.
        - app/assets/javascripts/listbox/index.js
    metadata:
      componentLabel: 'component:dropdown-collapsible-listbox'
      pajamasCompliant: true

  - id: okay-redirect-listbox-helper
    pattern: gl_redirect_listbox_tag
    message: Correct usage of gl_redirect_listbox_tag helper.
    languages:
      - generic
    severity: INFO
    paths:
      include:
        - "*.haml"
        - "*.rb"
      exclude:
        - app/helpers/listbox_helper.rb
    metadata:
      componentLabel: 'component:dropdown-collapsible-listbox'
      pajamasCompliant: true
      epic: "https://gitlab.com/groups/gitlab-org/-/epics/7551"

  - id: listbox-users-select
    patterns:
      - pattern-inside: 'import ...;'
      - pattern: "users_select'"
    message: |
      Detected usage of deprecated UsersSelect inside JS/Vue file.
    languages:
      - generic
    severity: ERROR
    paths:
      include:
        - "*.js"
        - "*.vue"
    metadata:
      componentLabel: 'component:dropdown-collapsible-listbox'
      pajamasCompliant: false
      epic: "https://gitlab.com/groups/gitlab-org/-/epics/8531"

  - id: listbox-deprecated-jquery-dropdown
    patterns:
      - pattern-inside: 'import ...;'
      - pattern: "deprecated_jquery_dropdown'"
    message: |
      Detected usage of deprecated JQuery Dropdown inside JS/Vue file.
    languages:
      - generic
    severity: ERROR
    paths:
      include:
        - "*.js"
        - "*.vue"
      exclude:
        # listbox-users-select
        - 'app/assets/javascripts/users_select/index.js'
        # listbox-ref-selector
        - 'app/assets/javascripts/ref_select_dropdown.js'
        - 'app/assets/javascripts/pages/projects/project.js'
        # listbox-timezone-dropdown
        - 'app/assets/javascripts/pages/projects/pipeline_schedules/shared/components/timezone_dropdown.js'
    metadata:
      componentLabel: 'component:dropdown-collapsible-listbox'
      pajamasCompliant: false
      epic: "https://gitlab.com/groups/gitlab-org/-/epics/4284"

  - id: listbox-ref-selector
    pattern-either:
      - pattern-regex: render.+shared/ref_dropdown
      - pattern-regex: render.+shared/ref_switcher
    message: |
      Detected usage of deprecated Ref Selector inside HAML file.
      These usages can be replaced with our shared ref selector.
    languages:
      - generic
    severity: ERROR
    paths:
      include:
        - "*.haml"
    metadata:
      componentLabel: 'component:dropdown-collapsible-listbox'
      pajamasCompliant: false
      epic: "https://gitlab.com/groups/gitlab-org/-/epics/8528"

  - id: listbox-timezone-dropdown
    patterns:
      - pattern-inside: 'import ...;'
      - pattern: "timezone_dropdown'"
    message: |
      Detected usage of deprecated Timezone Dropdown please use the
      timezone_dropdown.vue component instead.
    languages:
      - generic
    severity: ERROR
    paths:
      include:
        - "*.js"
        - "*.vue"
    metadata:
      componentLabel: 'component:dropdown-collapsible-listbox'
      pajamasCompliant: false
      epic: "https://gitlab.com/groups/gitlab-org/-/epics/8530"

  - id: listbox-selecttwo
    pattern: "'select2/select2'"
    message: |
      Detected usage of deprecated select2 inside JS/Vue file.
    languages:
      - generic
    severity: ERROR
    paths:
      include:
        - "*.js"
        - "*.vue"
    metadata:
      componentLabel: 'component:dropdown-collapsible-listbox'
      pajamasCompliant: false
      epic: "https://gitlab.com/groups/gitlab-org/-/epics/8738"

  - id: listbox-selecttwo-haml-ruby
    pattern-regex: select2
    message: |
      Detected usage of deprecated select2 inside HAML/Ruby file.
    languages:
      - generic
    severity: ERROR
    paths:
      include:
        - "*.haml"
        - "*.rb"
    metadata:
      componentLabel: 'component:dropdown-collapsible-listbox'
      pajamasCompliant: false
      epic: "https://gitlab.com/groups/gitlab-org/-/epics/8738"
