# Component rules

Have a look at the semgrep documentation on [how to write custom rules](https://semgrep.dev/docs/writing-rules/overview/)

As of now we distinguish between two kind of rules, by `severity`:

1. `WARNING`, these are rules that should hint towards the use of other components,
   for example replacing a deprecated Rails helper with a ViewComponent, or a deprecated Vue Component with a Pajamas compliant one.
2. `INFO`, these are rules that detect usage of Pajamas compliant components, we want to utilize them to track Pajamas adoption over time.

It seems to be a good practice to write separate rules for HAML and Vue / JS in order to keep rules simpler.
Even though semgrep seems to support proper parsing for both Vue / JS files,
the `generic` "language" and scanning with regexes searches seems powerful/precise enough for now.

Paths used in rules (e.g., `exclude`) must be written relative to the GitLab repository root.

## Example WARNING rule

The message should hint towards correct usage and might contain caveats.

```yaml
  - id: banner-class-haml
    patterns:
      - pattern-regex: \.gl-banner
    message: |
      Detected usage of `gl-banner` class inside HAML file.
      Please use the Pajamas::BannerComponent ViewComponent instead.
      Note: Sometimes the gl-banner class is mistakenly used for Empty states,
      so you might want to use one of those instead.
    languages:
      - generic
    severity: WARNING
    paths:
      include:
        - "*.haml"
      exclude:
        # The class usage is fine inside of the ViewComponent definition
        - 'app/components/pajamas/banner_component.html.haml'
    metadata:
      pajamasCompliant: false
```

## Example INFO rule

```yaml
  - id: okay-banner-vue-component
    patterns:
       - pattern-inside: import ... from '@gitlab/ui'
       - pattern: GlBanner
    message: Correct usage of GlBanner gitlab-ui component.
    languages:
       - generic
    severity: INFO
    paths:
       include:
          - "*.js"
          - "*.vue"
    metadata:
       pajamasCompliant: true
```
