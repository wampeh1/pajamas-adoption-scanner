const toFullId = (id) => `pas.rules.components.foo.${id}`;
const makeResult = ({
  severity = 'ERROR',
  id = 'bad-button',
  componentLabel = 'component:button',
  pajamasCompliant = false,
  epic = null,
  path = 'some/path.vue',
  group = 'group::foundations',
} = {}) => ({
  check_id: toFullId(id),
  end: {
    col: 22,
    line: 46,
    offset: 2542,
  },
  extra: {
    lines: 'some code',
    message: 'some message',
    metadata: {
      componentLabel,
      pajamasCompliant,
      epic,
    },
    severity,
    group,
  },
  path,
  start: {
    col: 14,
    line: 46,
    offset: 2534,
  },
});

const makeRule = ({
  id,
  severity = 'ERROR',
  message = `message for ${id}`,
  componentLabel = 'component:button',
  pajamasCompliant = false,
  epic = null,
} = {}) => ({
  id: toFullId(id),
  message,
  severity,
  metadata: {
    componentLabel,
    pajamasCompliant,
    epic,
  },
});

export const invalidScans = [undefined, null, {}, { results: [{}] }];

export const getEmptyScan = () => ({ results: [] });

export const getMinimalScan = () => ({ results: [makeResult()] });

export const getTypicalScan = () => ({
  results: [
    {},
    { path: 'different/file_type.js' },
    { id: 'okay-button', severity: 'INFO', pajamasCompliant: true },
    { id: 'bad-alert', componentLabel: 'component:alert' },
    { group: 'group::code review', path: 'mr/mr.vue' },
    {
      id: 'bad-spinner',
      componentLabel: 'component:spinner',
      epic: 'https://gitlab.test/epics/1234',
    },
    {
      id: 'bad-table',
      severity: 'WARNING',
      pajamasCompliant: false,
      componentLabel: 'component:table',
    },
  ].map(makeResult),
});

export const getHistory = () => [
  {
    date: '2021-01-01',
    summary: {
      'bad-alert': 1,
      'bad-button': 1,
      'okay-button': 1,
      'bad-table': 5,
    },
  },
  {
    date: '2022-02-02',
    summary: {
      'bad-alert': 2,
      'bad-button': 2,
      'okay-button': 1,
      'okay-label': 2,
      'bad-table': 7,
    },
  },
];

export const getScanSummary = () => ({
  current: {
    file: 'current_2022-10-27_abc123.json',
  },
  history: getHistory(),
});

export const getRules = () =>
  [
    { id: 'bad-button' },
    { id: 'okay-button', pajamasCompliant: true, severity: 'INFO' },
    { id: 'bad-alert', componentLabel: 'component:alert' },
    {
      id: 'okay-alert',
      componentLabel: 'component:alert',
      pajamasCompliant: true,
      severity: 'INFO',
    },
    { id: 'bad-label', componentLabel: 'component:label' },
    {
      id: 'okay-label',
      componentLabel: 'component:label',
      pajamasCompliant: true,
      severity: 'INFO',
    },
    {
      id: 'bad-spinner',
      componentLabel: 'component:spinner',
      epic: 'https://gitlab.test/epics/1234',
    },
    {
      id: 'bad-table',
      componentLabel: 'component:table',
      severity: 'WARNING',
      pajamasCompliant: false,
    },
  ].reduce((acc, rule) => {
    acc.set(rule.id, makeRule(rule));
    return acc;
  }, new Map());
