const getCompliancePercentageFor = ({ compliantFindings, defiantFindings }) => {
  const sum = compliantFindings + defiantFindings;

  // prettier-ignore
  return sum > 0 ?
    `${Number((100 * (compliantFindings / sum)).toFixed(2))}%`
  : `100%`;
};

const getFindingsByRuleIdMap = (scan) =>
  scan.results.reduce((acc, finding) => {
    if (!acc.has(finding.check_id)) acc.set(finding.check_id, 0);
    acc.set(finding.check_id, acc.get(finding.check_id) + 1);
    return acc;
  }, new Map());

/**
 * Return the rules-by-id table data props (fields, items, primaryKey) for the
 * given scan and rules
 *
 * @param {Object} The JSON output of a semgrep scan.
 * @param {Promise<Map<string, Rule>>} A map of rule ids to rule definitions.
 * @returns {Object} The fields, items and primaryKey props for a table.
 */
export const allRulesTableDataProps = (scan, rules) => {
  const fields = [
    { key: 'id', label: 'Rule ID', sortable: false },
    { key: 'severity', label: 'Severity', sortable: true },
    { key: 'componentLabel', label: 'Component label', sortable: true },
    { key: 'message', label: 'Message', sortable: false },
    {
      key: 'migrationGuide',
      label: 'Migration guide',
      sortable: true,
    },
    { key: 'findings', label: 'Findings', sortable: true },
  ].map((field) => ({ ...field, thClass: 'gl-white-space-nowrap' }));

  const findingsByRuleId = getFindingsByRuleIdMap(scan);

  const items = [...rules.values()].map((rule) => {
    const { epic, componentLabel } = rule.metadata;

    return {
      id: rule.id,
      message: rule.message,
      migrationGuide: epic,
      componentLabel,
      severity: rule.severity,
      findings: findingsByRuleId.get(rule.id) ?? 0,
    };
  });

  return { primaryKey: fields[0].key, fields, items };
};

/**
 * Return the rules-by-component table data props (fields, items, primaryKey)
 * for the given scan and rules.
 *
 * @param {Object} The JSON output of a semgrep scan.
 * @param {Promise<Map<string, Rule>>} A map of rule ids to rule definitions.
 * @returns {Object} The fields, items and primaryKey props for a table.
 */
export const componentRulesTableDataProps = (scan, rules) => {
  const fields = [
    { key: 'componentLabel', label: 'Component label', sortable: true },
    { key: 'defiantFindings', label: 'Not adopted findings', sortable: true },
    { key: 'warningFindings', label: 'Warning findings', sortable: true },
    { key: 'compliantFindings', label: 'Adopted findings', sortable: true },
    { key: 'totalFindings', label: 'Total findings', sortable: true },
    { key: 'compliancePercentage', label: 'Adoption percentage', sortable: true },
  ];

  const findingsByRuleId = getFindingsByRuleIdMap(scan);

  const itemsMap = [...rules.values()].reduce((acc, rule) => {
    const { componentLabel } = rule.metadata;
    if (!acc.has(componentLabel))
      acc.set(componentLabel, {
        componentLabel,
        defiantFindings: 0,
        warningFindings: 0,
        compliantFindings: 0,
        totalFindings: 0,
        compliancePercentage: 0,
      });

    const item = acc.get(componentLabel);
    const findings = findingsByRuleId.get(rule.id) ?? 0;
    item.totalFindings += findings;

    switch (rule.severity) {
      case 'INFO':
        item.compliantFindings += findings;
        break;
      case 'WARNING':
        item.warningFindings += findings;
        break;
      case 'ERROR':
        item.defiantFindings += findings;
        break;
    }

    item.compliancePercentage = getCompliancePercentageFor(item);

    return acc;
  }, new Map());

  return { primaryKey: fields[0].key, fields, items: [...itemsMap.values()] };
};

/**
 * Return the rules-by-group table data props (fields, items, primaryKey)
 * for the given scan and rules.
 *
 * Though, this table is more useful as a table about findings than rules,
 * since rules cannot be group-specific, unlike component labels.
 *
 * @param {Object} The JSON output of a semgrep scan.
 * @param {Promise<Map<string, Rule>>} A map of rule ids to rule definitions.
 * @returns {Object} The fields, items and primaryKey props for a table.
 */
export const groupRulesTableDataProps = (scan) => {
  const fields = [
    { key: 'groupLabel', label: 'Group label', sortable: true },
    { key: 'defiantFindings', label: 'Not adopted findings', sortable: true },
    { key: 'warningFindings', label: 'Warning findings', sortable: true },
    { key: 'compliantFindings', label: 'Adopted findings', sortable: true },
    { key: 'totalFindings', label: 'Total findings', sortable: true },
    { key: 'compliancePercentage', label: 'Adoption percentage', sortable: true },
  ];

  const itemsMap = [...scan.results].reduce((acc, finding) => {
    const { group } = finding.extra;
    if (!acc.has(group))
      acc.set(group, {
        group,
        defiantFindings: 0,
        warningFindings: 0,
        compliantFindings: 0,
        totalFindings: 0,
      });

    const item = acc.get(group);

    switch (finding.extra.severity) {
      case 'INFO':
        item.compliantFindings += 1;
        break;
      case 'WARNING':
        item.warningFindings += 1;
        break;
      case 'ERROR':
        item.defiantFindings += 1;
        break;
    }

    item.totalFindings = item.defiantFindings + item.warningFindings + item.compliantFindings;
    item.compliancePercentage = getCompliancePercentageFor(item);

    return acc;
  }, new Map());

  const items = Array.from(itemsMap.values());

  return { primaryKey: fields[0].key, fields, items };
};
