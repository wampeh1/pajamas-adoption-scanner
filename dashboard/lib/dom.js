import Vue from 'vue';

export const domFromComponent = (Component, props) => {
  const instance = new Vue({
    render(h) {
      return h(Component, { props });
    },
  });

  instance.$mount();

  const dom = instance.$el.cloneNode(true);

  instance.$destroy();

  return dom;
};
