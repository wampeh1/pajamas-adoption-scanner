import { filter, parse } from 'scim2-parse-filter';

const returnTrue = () => true;

export const getScim2Filter = (query) => {
  let parseError = '';
  let filterFn = returnTrue;

  if (query.trim()) {
    try {
      filterFn = filter(parse(query));
    } catch (e) {
      parseError = `Invalid filter: ${e.message}`;
    }
  }

  return {
    filterFn,
    parseError,
  };
};
