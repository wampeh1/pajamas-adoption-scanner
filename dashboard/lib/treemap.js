import defaults from 'lodash/defaults';
import { shortId } from 'shared/rules.js';
import { COLOR_INFO, COLOR_WARNING, COLOR_ERROR } from './constants';
import {
  GROUP_BY_COMPONENT,
  GROUP_BY_SEVERITY,
  GROUP_BY_FILE_TYPE,
  GROUP_BY_PAJAMAS_COMPLIANCE,
  GROUP_BY_GROUP,
  GROUP_BY_MIGRATION_GUIDE,
} from '~/components/group_by.vue';

const isLeafNode = (node) => !Object.hasOwn(node, 'children');

/**
 * Returns the maximum number of levels in an array of nodes.
 *
 * For example, in pseudocode:
 *   treeLevels([]) === 1
 *   treeLevels([leaf, leaf]) === 2
 *   treeLevels([parent([leaf, leaf]), leaf]) === 3
 *
 * @param {Array} nodes An array of tree nodes.
 * @returns {number} The tree's levels.
 */
const treeLevels = (nodes) =>
  1 + Math.max(0, ...nodes.map((node) => (isLeafNode(node) ? 1 : treeLevels(node.children))));

const getLevelsConfig = (numLevels) => {
  const validLevels = [1, 2, 3];
  if (!validLevels.includes(numLevels)) {
    throw new Error(`Number of levels must be one of ${validLevels.join(', ')}; got ${numLevels}`);
  }

  const topLevel = {
    itemStyle: {
      gapWidth: numLevels === 2 ? 1 : 5,
    },
    upperLabel: {
      show: true,
      formatter: '{c} total findings',
    },
  };

  const componentLevel = {
    itemStyle: {
      borderColor: '#eee',
      borderWidth: 2,
      gapWidth: 1,
    },
    upperLabel: {
      show: true,
      formatter: '{b}: {c} findings',
    },
    emphasis: {
      itemStyle: {
        borderColor: '#aaa',
      },
    },
  };

  const ruleLevel = {
    itemStyle: {
      gapWidth: 1,
    },
    label: {
      position: 'insideTopLeft',
      align: 'left',
      verticalAlign: 'top',
      lineHeight: 16,
      rich: {
        code: {
          fontFamily: 'GitLab Mono',
          fontWeight: 'bold',
        },
        label: {
          backgroundColor: 'rgba(0,0,0,0.3)',
          color: '#fff',
          borderRadius: 2,
          padding: [2, 4],
        },
      },
      formatter(params) {
        const { name, value, severity, migrationGuide } = params.data;
        const formatted = `{code|${name}}\n${value} findings`;

        if (severity === 'ERROR' && !migrationGuide) {
          return `${formatted}\n{label|Migration guide pending}`;
        }

        return formatted;
      },
    },
  };

  if (numLevels === 1) return [topLevel];
  if (numLevels === 2) return [topLevel, ruleLevel];
  if (numLevels === 3) return [topLevel, componentLevel, ruleLevel];
};

const scanToTreeMapData = (scan, groupForFinding) => {
  // Map<groupName, Map<checkId, nodeLike>>
  const groups = new Map();

  scan.results.forEach((result) => {
    const { check_id: checkId } = result;
    const groupName = groupForFinding(result);

    let node;
    let group;

    if (groups.has(groupName)) {
      group = groups.get(groupName);

      if (group.has(checkId)) {
        node = group.get(checkId);
        node.findingIds.push(result.id);
      }
    } else {
      group = new Map();
      groups.set(groupName, group);
    }

    if (!node) {
      const name = shortId(result.check_id);
      node = { name, result, findingIds: [result.id] };
      group.set(checkId, node);
    }
  });

  return Array.from(groups.entries()).map(([groupName, nodeMap]) => ({
    name: groupName,
    children: Array.from(nodeMap.values()).map(({ name, result, findingIds }) => {
      const { severity } = result.extra;
      const { epic } = result.extra.metadata;
      // prettier-ignore
      const color =
        severity === 'INFO' ?
          COLOR_INFO
        : severity === 'WARNING' ?
          COLOR_WARNING
        : COLOR_ERROR;

      return {
        name,
        message: result.extra.message,
        itemStyle: { color },
        severity,
        migrationGuide: epic,
        value: findingIds.length,
        findingIds,
      };
    }),
  }));
};

const scanToTreeMapSeriesGrouped = (name, groupForFinding) => (scan) => {
  const data = scanToTreeMapData(scan, groupForFinding);

  return {
    name,
    levels: getLevelsConfig(treeLevels(data)),
    data,
  };
};

const scanToTreeMapSeriesFlat = (scan) => {
  const [root] = scanToTreeMapData(scan, () => 'root');
  const data = root?.children ?? [];

  return {
    name: 'Rules',
    levels: getLevelsConfig(treeLevels(data)),
    data,
  };
};

export const defaultOptions = {
  type: 'treemap',
  width: '100%',
  height: '100%',
  animation: false,
  nodeClick: false,
  breadcrumb: {
    show: false,
  },
};

const bySeverity = ({ extra }) => extra.severity;
const byFileType = ({ path }) => path.split('.').reverse()[0];
const byPajamasCompliance = ({ extra }) =>
  extra.metadata.pajamasCompliant ? 'Adopted' : 'Not adopted';
const byComponent = ({
  extra: {
    metadata: { componentLabel },
  },
}) => componentLabel.slice(componentLabel.indexOf(':') + 1);
const byGroup = ({ extra }) => extra.group.replace(/^group::/, '');
const byMigrationGuide = ({ extra }) => (extra.metadata.epic ? 'Yes' : 'No');

/**
 * Transforms the JSON output of a semgrep scan into a series object for
 * charting.
 * @param {Object} The JSON output of a semgrep scan.
 * @returns {Object} A series object consumable by eCharts.
 */
export const scanToTreeMapSeries = (scan, { groupBy } = {}) => {
  let transform = scanToTreeMapSeriesFlat;
  if (groupBy === GROUP_BY_COMPONENT)
    transform = scanToTreeMapSeriesGrouped('Components', byComponent);
  else if (groupBy === GROUP_BY_SEVERITY)
    transform = scanToTreeMapSeriesGrouped('Severities', bySeverity);
  else if (groupBy === GROUP_BY_FILE_TYPE)
    transform = scanToTreeMapSeriesGrouped('File types', byFileType);
  else if (groupBy === GROUP_BY_PAJAMAS_COMPLIANCE)
    transform = scanToTreeMapSeriesGrouped('Pajamas adoption', byPajamasCompliance);
  else if (groupBy === GROUP_BY_GROUP) transform = scanToTreeMapSeriesGrouped('Groups', byGroup);
  else if (groupBy === GROUP_BY_MIGRATION_GUIDE)
    transform = scanToTreeMapSeriesGrouped('Migration guide', byMigrationGuide);

  return defaults(transform(scan), defaultOptions);
};

function* leafNodeIterator(node) {
  if (isLeafNode(node)) {
    yield node;
    return;
  }

  for (const child of node.children) {
    yield* leafNodeIterator(child);
  }
}

export const findingsForNode = (scan, node) => {
  if (!scan) return [];
  if (!node) return scan.results;

  const findingIdsInNode = new Set();

  for (const leaf of leafNodeIterator(node)) {
    for (const id of leaf.findingIds) {
      findingIdsInNode.add(id);
    }
  }

  return scan.results.filter((result) => findingIdsInNode.has(result.id));
};
