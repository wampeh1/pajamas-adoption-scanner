export const GITLAB_REPO_WEB_URL = 'https://gitlab.com/gitlab-org/gitlab';

export {
  GREEN_400 as COLOR_INFO,
  ORANGE_400 as COLOR_WARNING,
  RED_500 as COLOR_ERROR,
} from '@gitlab/ui/dist/tokens/js/tokens';
