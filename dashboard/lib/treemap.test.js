import { scanToTreeMapSeries, defaultOptions, findingsForNode } from './treemap';
import { enhanceScan } from './api';
import {
  invalidScans,
  getEmptyScan,
  getMinimalScan,
  getTypicalScan,
} from '~/test_support/mock_scans';
import {
  groupByOptions,
  GROUP_BY_COMPONENT,
  GROUP_BY_SEVERITY,
  GROUP_BY_FILE_TYPE,
  GROUP_BY_PAJAMAS_COMPLIANCE,
  GROUP_BY_GROUP,
  GROUP_BY_MIGRATION_GUIDE,
  GROUP_BY_NOTHING,
} from '~/components/group_by.vue';

const groupBys = groupByOptions.map(({ value }) => value);

describe('scanToTreeMapSeries', () => {
  describe('common behaviours', () => {
    describe.each(groupBys)('given groupBy: %s', (groupBy) => {
      describe.each(invalidScans)('given invalid scan %p', (scan) => {
        it('throws', () => {
          expect(() => scanToTreeMapSeries(scan, { groupBy })).toThrow();
        });
      });

      describe('given an empty scan', () => {
        it('returns an empty data array', () => {
          expect(scanToTreeMapSeries(getEmptyScan(), { groupBy }).data).toEqual([]);
        });
      });

      describe('given valid scan', () => {
        it('sets defaults', () => {
          expect(scanToTreeMapSeries(getMinimalScan(), { groupBy })).toMatchObject(defaultOptions);
        });
      });
    });
  });

  describe.each`
    context               | groupBy                        | name                  | levels
    ${'no'}               | ${GROUP_BY_NOTHING}            | ${'Rules'}            | ${2}
    ${'component'}        | ${GROUP_BY_COMPONENT}          | ${'Components'}       | ${3}
    ${'severity'}         | ${GROUP_BY_SEVERITY}           | ${'Severities'}       | ${3}
    ${'file type'}        | ${GROUP_BY_FILE_TYPE}          | ${'File types'}       | ${3}
    ${'Pajamas adoption'} | ${GROUP_BY_PAJAMAS_COMPLIANCE} | ${'Pajamas adoption'} | ${3}
    ${'group'}            | ${GROUP_BY_GROUP}              | ${'Groups'}           | ${3}
    ${'migration guide'}  | ${GROUP_BY_MIGRATION_GUIDE}    | ${'Migration guide'}  | ${3}
  `('given $context grouping', ({ groupBy, levels, name }) => {
    it('sets the name and levels correctly', () => {
      const series = scanToTreeMapSeries(getMinimalScan(), { groupBy });

      expect(series.name).toBe(name);
      expect(series.levels.length).toBe(levels);
    });
  });

  it('no grouping returns the correct structure for a typical scan', () => {
    expect(scanToTreeMapSeries(getTypicalScan()).data).toMatchObject([
      {
        name: 'bad-button',
        value: 3,
      },
      {
        name: 'okay-button',
        value: 1,
      },
      {
        name: 'bad-alert',
        value: 1,
      },
      {
        name: 'bad-spinner',
        value: 1,
      },
      {
        name: 'bad-table',
        value: 1,
      },
    ]);
  });

  it('component grouping returns the correct structure for a typical scan', () => {
    expect(
      scanToTreeMapSeries(getTypicalScan(), { groupBy: GROUP_BY_COMPONENT }).data,
    ).toMatchObject([
      {
        name: 'button',
        children: [
          {
            name: 'bad-button',
            value: 3,
          },
          {
            name: 'okay-button',
            value: 1,
          },
        ],
      },
      {
        name: 'alert',
        children: [
          {
            name: 'bad-alert',
            value: 1,
          },
        ],
      },
      {
        name: 'spinner',
        children: [
          {
            name: 'bad-spinner',
            value: 1,
          },
        ],
      },
      {
        name: 'table',
        children: [
          {
            name: 'bad-table',
            value: 1,
          },
        ],
      },
    ]);
  });

  it('severity grouping returns the correct structure for a typical scan', () => {
    expect(
      scanToTreeMapSeries(getTypicalScan(), { groupBy: GROUP_BY_SEVERITY }).data,
    ).toMatchObject([
      {
        name: 'ERROR',
        children: [
          {
            name: 'bad-button',
            value: 3,
          },
          {
            name: 'bad-alert',
            value: 1,
          },
          {
            name: 'bad-spinner',
            value: 1,
          },
        ],
      },
      {
        name: 'INFO',
        children: [
          {
            name: 'okay-button',
            value: 1,
          },
        ],
      },
      {
        name: 'WARNING',
        children: [
          {
            name: 'bad-table',
            value: 1,
          },
        ],
      },
    ]);
  });

  it('file type grouping returns the correct structure for a typical scan', () => {
    expect(
      scanToTreeMapSeries(getTypicalScan(), { groupBy: GROUP_BY_FILE_TYPE }).data,
    ).toMatchObject([
      {
        name: 'vue',
        children: [
          {
            name: 'bad-button',
            value: 2,
          },
          {
            name: 'okay-button',
            value: 1,
          },
          {
            name: 'bad-alert',
            value: 1,
          },
          {
            name: 'bad-spinner',
            value: 1,
          },
          {
            name: 'bad-table',
            value: 1,
          },
        ],
      },
      {
        name: 'js',
        children: [
          {
            name: 'bad-button',
            value: 1,
          },
        ],
      },
    ]);
  });

  it('Pajamas adoption grouping returns the correct structure for a typical scan', () => {
    expect(
      scanToTreeMapSeries(getTypicalScan(), { groupBy: GROUP_BY_PAJAMAS_COMPLIANCE }).data,
    ).toMatchObject([
      {
        name: 'Not adopted',
        children: [
          {
            name: 'bad-button',
            value: 3,
          },
          {
            name: 'bad-alert',
            value: 1,
          },
          {
            name: 'bad-spinner',
            value: 1,
          },
          {
            name: 'bad-table',
            value: 1,
          },
        ],
      },
      {
        name: 'Adopted',
        children: [
          {
            name: 'okay-button',
            value: 1,
          },
        ],
      },
    ]);
  });

  it('Group grouping returns the correct structure for a typical scan', () => {
    expect(scanToTreeMapSeries(getTypicalScan(), { groupBy: GROUP_BY_GROUP }).data).toMatchObject([
      {
        name: 'foundations',
        children: [
          {
            name: 'bad-button',
            value: 2,
          },
          {
            name: 'okay-button',
            value: 1,
          },
          {
            name: 'bad-alert',
            value: 1,
          },
          {
            name: 'bad-spinner',
            value: 1,
          },
          {
            name: 'bad-table',
            value: 1,
          },
        ],
      },
      {
        name: 'code review',
        children: [
          {
            name: 'bad-button',
            value: 1,
          },
        ],
      },
    ]);
  });

  it('Migration guide grouping returns the correct structure for a typical scan', () => {
    expect(
      scanToTreeMapSeries(getTypicalScan(), { groupBy: GROUP_BY_MIGRATION_GUIDE }).data,
    ).toMatchObject([
      {
        name: 'No',
        children: [
          {
            name: 'bad-button',
            value: 3,
          },
          {
            name: 'okay-button',
            value: 1,
          },
          {
            name: 'bad-alert',
            value: 1,
          },
          {
            name: 'bad-table',
            value: 1,
          },
        ],
      },
      {
        name: 'Yes',
        children: [
          {
            name: 'bad-spinner',
            value: 1,
          },
        ],
      },
    ]);
  });
});

describe('findingsForNode', () => {
  it('returns no findings given no scan', () => {
    expect(findingsForNode()).toEqual([]);
  });

  it('returns all findings given no node', () => {
    const scan = enhanceScan(getTypicalScan());

    expect(findingsForNode(scan)).toBe(scan.results);
  });

  it('returns the correct findings given a leaf node', () => {
    const scan = enhanceScan(getTypicalScan());
    const series = scanToTreeMapSeries(scan);
    const [node] = series.data;

    expect(node.name).toBe('bad-button');
    expect(findingsForNode(scan, node)).toEqual([
      scan.results[0],
      scan.results[1],
      scan.results[4],
    ]);
  });

  it('returns the correct findings given a group node', () => {
    const scan = enhanceScan(getTypicalScan());
    const series = scanToTreeMapSeries(scan, { groupBy: GROUP_BY_SEVERITY });
    const [node] = series.data;

    expect(node.name).toBe('ERROR');
    expect(findingsForNode(scan, node)).toEqual([
      scan.results[0],
      scan.results[1],
      // Note index 2 is missing, it's in another group
      scan.results[3],
      scan.results[4],
      scan.results[5],
    ]);
  });
});
