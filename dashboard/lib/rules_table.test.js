import {
  allRulesTableDataProps,
  componentRulesTableDataProps,
  groupRulesTableDataProps,
} from './rules_table';
import { getTypicalScan, getRules } from '~/test_support/mock_scans';

describe('allRulesTableDataProps', () => {
  it('returns correct props for typical scan', () => {
    expect(allRulesTableDataProps(getTypicalScan(), getRules())).toEqual({
      primaryKey: 'id',
      fields: [
        { key: 'id', label: 'Rule ID', thClass: 'gl-white-space-nowrap', sortable: false },
        { key: 'severity', label: 'Severity', thClass: 'gl-white-space-nowrap', sortable: true },
        {
          key: 'componentLabel',
          label: 'Component label',
          thClass: 'gl-white-space-nowrap',
          sortable: true,
        },
        { key: 'message', label: 'Message', thClass: 'gl-white-space-nowrap', sortable: false },
        {
          key: 'migrationGuide',
          label: 'Migration guide',
          thClass: 'gl-white-space-nowrap',
          sortable: true,
        },
        { key: 'findings', label: 'Findings', thClass: 'gl-white-space-nowrap', sortable: true },
      ],
      items: [
        {
          id: 'pas.rules.components.foo.bad-button',
          componentLabel: 'component:button',
          findings: 3,
          message: 'message for bad-button',
          migrationGuide: null,
          severity: 'ERROR',
        },
        {
          id: 'pas.rules.components.foo.okay-button',
          componentLabel: 'component:button',
          findings: 1,
          message: 'message for okay-button',
          migrationGuide: null,
          severity: 'INFO',
        },
        {
          id: 'pas.rules.components.foo.bad-alert',
          componentLabel: 'component:alert',
          findings: 1,
          message: 'message for bad-alert',
          migrationGuide: null,
          severity: 'ERROR',
        },
        {
          id: 'pas.rules.components.foo.okay-alert',
          componentLabel: 'component:alert',
          findings: 0,
          message: 'message for okay-alert',
          migrationGuide: null,
          severity: 'INFO',
        },
        {
          id: 'pas.rules.components.foo.bad-label',
          componentLabel: 'component:label',
          findings: 0,
          message: 'message for bad-label',
          migrationGuide: null,
          severity: 'ERROR',
        },
        {
          id: 'pas.rules.components.foo.okay-label',
          componentLabel: 'component:label',
          findings: 0,
          message: 'message for okay-label',
          migrationGuide: null,
          severity: 'INFO',
        },
        {
          id: 'pas.rules.components.foo.bad-spinner',
          componentLabel: 'component:spinner',
          findings: 1,
          message: 'message for bad-spinner',
          migrationGuide: 'https://gitlab.test/epics/1234',
          severity: 'ERROR',
        },
        {
          id: 'pas.rules.components.foo.bad-table',
          componentLabel: 'component:table',
          findings: 1,
          message: 'message for bad-table',
          migrationGuide: null,
          severity: 'WARNING',
        },
      ],
    });
  });
});

describe('componentRulesTableDataProps', () => {
  it('returns correct props for typical scan', () => {
    expect(componentRulesTableDataProps(getTypicalScan(), getRules())).toEqual({
      primaryKey: 'componentLabel',
      fields: [
        { key: 'componentLabel', label: 'Component label', sortable: true },
        { key: 'defiantFindings', label: 'Not adopted findings', sortable: true },
        { key: 'warningFindings', label: 'Warning findings', sortable: true },
        { key: 'compliantFindings', label: 'Adopted findings', sortable: true },
        { key: 'totalFindings', label: 'Total findings', sortable: true },
        { key: 'compliancePercentage', label: 'Adoption percentage', sortable: true },
      ],
      items: [
        {
          componentLabel: 'component:button',
          compliantFindings: 1,
          warningFindings: 0,
          defiantFindings: 3,
          totalFindings: 4,
          compliancePercentage: '25%',
        },
        {
          componentLabel: 'component:alert',
          compliantFindings: 0,
          warningFindings: 0,
          defiantFindings: 1,
          totalFindings: 1,
          compliancePercentage: '0%',
        },
        {
          componentLabel: 'component:label',
          compliantFindings: 0,
          warningFindings: 0,
          defiantFindings: 0,
          totalFindings: 0,
          compliancePercentage: '100%',
        },
        {
          componentLabel: 'component:spinner',
          compliantFindings: 0,
          warningFindings: 0,
          defiantFindings: 1,
          totalFindings: 1,
          compliancePercentage: '0%',
        },
        {
          componentLabel: 'component:table',
          compliantFindings: 0,
          warningFindings: 1,
          defiantFindings: 0,
          totalFindings: 1,
          compliancePercentage: '100%',
        },
      ],
    });
  });
});

describe('groupRulesTableDataProps', () => {
  it('returns correct props for typical scan', () => {
    expect(groupRulesTableDataProps(getTypicalScan())).toMatchObject({
      primaryKey: 'groupLabel',
      fields: [
        { key: 'groupLabel', label: 'Group label', sortable: true },
        { key: 'defiantFindings', label: 'Not adopted findings', sortable: true },
        { key: 'warningFindings', label: 'Warning findings', sortable: true },
        { key: 'compliantFindings', label: 'Adopted findings', sortable: true },
        { key: 'totalFindings', label: 'Total findings', sortable: true },
        { key: 'compliancePercentage', label: 'Adoption percentage', sortable: true },
      ],
      items: [
        {
          group: 'group::foundations',
          compliantFindings: 1,
          warningFindings: 1,
          defiantFindings: 4,
          totalFindings: 6,
          compliancePercentage: '20%',
        },
        {
          group: 'group::code review',
          compliantFindings: 0,
          warningFindings: 0,
          defiantFindings: 1,
          totalFindings: 1,
          compliancePercentage: '0%',
        },
      ],
    });
  });
});
