import memoize from 'lodash/memoize';
import { makeUrl } from './url';

const fetchJSON = async (path) => {
  const response = await fetch(path);

  if (!response.ok) {
    const error = new Error(`"${response.url}": ${response.statusText}`);
    error.response = response;
    throw error;
  }

  return response.json();
};

/**
 * Get the data structure describing existing scans.
 */
export const getScansSummary = memoize(() => fetchJSON('scans.json'));

/**
 * Get the data structure describing current rules.
 * @returns {Promise<Map<string, Rule>>} A map of rule ids to rule definitions.
 */
export const getRules = memoize(async () => {
  const rules = await fetchJSON('rules.json');
  return rules.reduce((acc, rule) => {
    acc.set(rule.id, rule);
    return acc;
  }, new Map());
});

/**
 * Decorate scan data with extra fields useful for display logic.
 */
export const enhanceScan = (scan) => {
  scan.results.forEach((finding, i) => {
    finding.id = i;
  });

  return scan;
};

/**
 * Returns the JSON output of a semgrep scan, with some modifications.
 *
 * Modifications:
 * - Each finding is given a unique `id`.
 */
export const getCurrentScan = async () => {
  const { current } = await getScansSummary();

  return enhanceScan(await fetchJSON(current.file));
};

/**
 * Search for an issue. Uses sessionStorage by default as a cache for results.
 *
 * @param {Object} options
 *     finding.
 * @param {string} options.componentLabel The component label associated with the
 *     finding.
 * @param {string} options.path The path of the finding's file.
 * @param {string} [options.projectFullPath] The project's full path.
 * @returns {{ iid: string, url: string }}
 */
export async function cachedFindIssueForPathAndLabel({
  componentLabel,
  path,
  projectFullPath = 'gitlab-org/gitlab',
  storage = sessionStorage,
}) {
  const storageKey = `${componentLabel}|${path}`;
  const localValue = storage.getItem(storageKey);
  let issue;
  if (localValue) {
    try {
      issue = JSON.parse(localValue);
    } catch (error) {
      // Value cannot be parsed for some reason, so throw it away.
      storage.removeItem(storageKey);
      throw error;
    }

    return issue;
  }

  const projectId = encodeURIComponent(projectFullPath);
  const url = makeUrl(`https://gitlab.com/api/v4/projects/${projectId}/issues`, {
    labels: componentLabel,
    state: 'opened',
    search: path.replace(/[A-Za-z0-9+/@]{50,}/, ' '),
    in: 'title',
  });

  const [fullIssue] = await fetchJSON(url);
  if (!fullIssue?.web_url) return null;

  issue = { url: fullIssue.web_url, iid: fullIssue.iid };
  storage.setItem(storageKey, JSON.stringify(issue));

  return issue;
}
