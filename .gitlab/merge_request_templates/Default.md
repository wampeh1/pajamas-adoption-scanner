## What does this MR do and why?

<!--
Describe in detail what your merge request does and why.

Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

## Review/run this locally

1. Copy the `pages in mr` job URL (must have succeeded)
1. Run `bin/review-mr.sh <job url>`
1. If there are rules changes, check the `compare findings` job log reports the
   expected changes
